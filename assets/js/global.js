function logout() {
    localStorage.removeItem('__user-data');
    localStorage.removeItem('__jwt-token');

    window.location.href = 'https://web-checkpoint.vercel.app/login/'
}
(function () {
    function checkLogin() {
        const jwt = localStorage.getItem('__jwt-token');

        if (!jwt || jwt === '') {
            window.location.href = 'https://web-checkpoint.vercel.app/login/'
        }
    }
    checkLogin()
})()